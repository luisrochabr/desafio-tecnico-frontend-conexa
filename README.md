<h1 align="center">Desafio Técnico Front-end - Conexa Saúde</h1>

<p align="center">
    <img src="https://img.shields.io/static/v1?label=react&message=framework&color=blue&style=for-the-badge&logo=REACT"/>
    <img src="http://img.shields.io/static/v1?label=TESTES&message=%3E2&color=GREEN&style=for-the-badge"/>
    <img src="http://img.shields.io/static/v1?label=STATUS&message=CONCLUIDO&color=GREEN&style=for-the-badge"/>
</p> 


## Pré-requisitos

:warning: [Node](https://nodejs.org/en/download/)
:warning: [Yarn](https://www.npmjs.com/package/yarn) ou [NPM](https://www.npmjs.com/package/npm)

## Linguagens e libs utilizadas :books:

- [React](https://reactjs.org/)
- [@mui/material](https://mui.com/)
- [styled-components](https://www.npmjs.com/package/styled-components/v/4.1.3)
- [react-virtualized](https://www.npmjs.com/package/react-virtualized)
- [react-router-dom](https://www.npmjs.com/package/react-router-dom)
- [git-commit-msg-linte](https://www.npmjs.com/package/git-commit-msg-linter)
- [react-hook-form](https://www.npmjs.com/package/react-hook-form)
- [react-toastify](https://www.npmjs.com/package/react-toastify)
## Como rodar a aplicação :arrow_forward:

No terminal, clone o projeto: 

```
git clone https://gitlab.com/luisrochabr/desafio-tecnico-frontend-conexa.git
```

Entre na pasta do projeto:

```
cd desafio-tecnico-frontend-conexa\frontend
```

Instale as dependências:

```
yarn install ou npm install
```

Renomeie o .env

```
mv .env.example .env.developemnt
```

Configure o endpoint da api no arquivo .env:

```
REACT_APP_API_URL=http://localhost:3333
```

Execute a aplicação:

```
yarn start ou npm run start
```

## Como rodar o backend :arrow_forward:

Acesse a pasta da aplicação:
```
cd ../backend
```

Instale as dependências:
```
yarn install ou npm install
```

Execute a aplicação:
```
yarn start ou npm run start
```
...para rodar o servidor na url [http://localhost:3333](http://localhost:3333).


### Usuário para teste: 
email|senha
| -------- | --------
| gandalf@mail.com | 123456

