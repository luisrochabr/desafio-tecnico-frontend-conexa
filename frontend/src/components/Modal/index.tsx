import { Close } from '@mui/icons-material'
import { Dialog, DialogContent, IconButton, Fade, FadeProps } from '@mui/material'
import { forwardRef, ReactElement, Ref } from 'react'
import { IModalProps } from '../../@types/layouts.interface'

const Transition = forwardRef(function Transition(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  props: FadeProps & { children?: ReactElement<any, any> },
  ref: Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})

const Modal = ({ children, isOpen, setIsOpen }: IModalProps) => {
  return (
    <Dialog
      fullWidth
      open={isOpen}
      maxWidth='md'
      scroll='body'
      onClose={() => setIsOpen(false)}
      TransitionComponent={Transition}
      onBackdropClick={() => setIsOpen(false)}
    >
      <DialogContent sx={{ px: { xs: 8, sm: 15 }, py: { xs: 8, sm: 12.5 }, position: 'relative' }}>
        <IconButton
          size='small'
          onClick={() => setIsOpen(false)}
          sx={{ position: 'absolute', right: '1rem', top: '1rem' }}
        >
          <Close />
        </IconButton>
        {children}
      </DialogContent>
    </Dialog>
  )
}
export default Modal
