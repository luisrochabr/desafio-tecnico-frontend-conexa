import { Stack } from '@mui/material'
import { InputProps } from '../../@types/layouts.interface'
import { StyledFormLabel, StyledMuiInput, IconHelper } from './styles'

import IconHelp from '../../assets/icon-helper.svg'

const Input = ({ id, label, icon, error, ...props }: InputProps) => {
  return (
    <Stack>
      <StyledFormLabel htmlFor={id} error={error}>
        {label}
        {icon && <IconHelper src={IconHelp} />}
      </StyledFormLabel>
      <StyledMuiInput error={error} {...props} />
    </Stack>
  )
}

export default Input
