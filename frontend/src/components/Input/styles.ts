import {
  FormLabel,
  FormLabelProps,
  Input as MuiInput,
  InputProps as MuiInputProps,
  styled
} from '@mui/material'

export const StyledFormLabel = styled(FormLabel)<FormLabelProps>`
  color: ${({ theme }) => theme.palette.text.primary};
  font-family: 'Nunito';
  font-size: 0.875rem;
  margin: 0;
  padding: 0;
`
export const StyledMuiInput = styled(MuiInput)<MuiInputProps>`
  margin: 0 !important;
  font-family: 'Nunito';
  font-style: italic;
  font-weight: 300;
  font-size: 0.875rem;
`
export const IconHelper = styled('img')`
  padding-left: 0.5rem;
`
