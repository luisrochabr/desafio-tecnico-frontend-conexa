// ** React Imports
import { useEffect } from 'react'

// ** MUI Imports
import { Box, Grid, Stack, Typography, Button } from '@mui/material'

// ** Third Party Imports
import { ListRowProps, ListRowRenderer } from 'react-virtualized'
import List from 'react-virtualized/dist/commonjs/List'

// ** Hooks Imports
import { useAuth } from '../../hooks/useAuth'
import { useConsultation } from '../../hooks/useConsultation'

// ** Components Imports
import NoSchedulings from '../../views/NoSchedulings'
import { ConsultationPageTitle } from '../../layouts/styles'

// const SchedulingsList = (schedulingsList: SchedulingDataType[]) => {
//   return (

//   )
// }
const Home = () => {
  // ** Hooks
  const { isAuthenticated } = useAuth()
  const { schedulingList, listAllSchedulings, listAllPatitients } = useConsultation()

  useEffect(() => {
    if (isAuthenticated) {
      listAllPatitients()
      listAllSchedulings()
    }
  }, [listAllPatitients, listAllSchedulings])

  const _rowRenderer: ListRowRenderer = ({ index, key, style }: ListRowProps) => {
    return (
      <Box key={key} style={style}>
        <Stack flexDirection='row' justifyContent='space-between'>
          <Box>
            <Typography variant='body1'>{`${schedulingList[index].patient?.first_name} ${schedulingList[index].patient?.last_name}`}</Typography>
            <Typography variant='body2' fontSize='12px'>
              {schedulingList[index]?.date}
            </Typography>
          </Box>
          <Box>
            <Button variant='contained'>Atender</Button>
          </Box>
        </Stack>
      </Box>
    )
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <Box
          sx={{
            display: 'flex',
            justifyContent: ['center', 'flex-start'],
            padding: theme => [theme.spacing(4.375), theme.spacing(7.5)]
          }}
        >
          <ConsultationPageTitle variant='h1'>Consultas</ConsultationPageTitle>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box
          mx={2}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Box sx={{ width: '100%', maxWidth: '520px' }}>
            <Box
              flexDirection='column'
              justifyContent='space-around'
              sx={{
                width: '100%',
                height: '100%'
              }}
            >
              <Box pb={2}>
                <Typography variant='body1' fontWeight='700'>
                  {schedulingList?.length > 0 ? schedulingList?.length : '0'} consultas agendadas
                </Typography>
              </Box>

              {schedulingList?.length ? (
                <List
                  autoWidth
                  width={520}
                  height={300}
                  rowHeight={50}
                  estimatedRowSize={50}
                  overscanRowCount={5}
                  isScrolling
                  rowCount={schedulingList.length}
                  rowRenderer={_rowRenderer}
                />
              ) : (
                <NoSchedulings />
              )}
            </Box>
          </Box>
        </Box>
      </Grid>
    </Grid>
  )
}

export default Home
