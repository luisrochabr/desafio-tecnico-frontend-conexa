import { styled } from '@mui/material/styles'
import { Box, BoxProps } from '@mui/material'

export const LoginIllustrationWrapper = styled(Box)<BoxProps>(() => ({
  display: 'flex',
  flex: 1,
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  height: '100%',
  paddingTop: '1rem'
}))
