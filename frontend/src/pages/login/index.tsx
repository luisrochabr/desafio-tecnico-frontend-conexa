// ** React Imports
import { useState } from 'react'

// ** MUI Imports
import {
  useMediaQuery,
  Grid,
  Box,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  Theme,
  Button
} from '@mui/material'

// ** Third Party Imports
import { useForm, Controller, SubmitHandler } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

// ** Types Imports
import { LoginDataType, loginSchema } from '../../@types/auth.types'

// ** Styled Imports
import { LoginIllustrationWrapper } from './styles'
import Input from '../../components/Input'

// ** Icons Imports
import EyeOutline from '@mui/icons-material/Visibility'
import EyeOffOutline from '@mui/icons-material/VisibilityOff'

// ** Assets Imports
import LoginIllustrationSrc from '../../assets/login-illustration.svg'
import { useAuth } from '../../hooks/useAuth'
import { LoginPageTitle } from '../../layouts/styles'

const Login = () => {
  // ** States
  const [showPassword, setShowPassword] = useState<boolean>(false)

  // ** Hook
  const auth = useAuth()

  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      email: 'gandalf@mail.com',
      password: '123456'
    },
    mode: 'onBlur',
    resolver: yupResolver(loginSchema)
  })

  // ** Vars
  const hidden = useMediaQuery((theme: Theme) => theme.breakpoints.down('sm'))

  // ** Functions
  const handleLogin: SubmitHandler<LoginDataType> = async (data: LoginDataType) => {
    auth.login(data)
  }

  const handleClickShowPassword = () => {
    setShowPassword(prevShowPassword => !prevShowPassword)
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <Box
          sx={{
            display: 'flex',
            justifyContent: ['center', 'flex-start'],
            padding: theme => [theme.spacing(4.375), theme.spacing(7.5)]
          }}
        >
          <LoginPageTitle variant='h1'>Faça Login</LoginPageTitle>
        </Box>
      </Grid>
      {!hidden ? (
        <Grid item sm={6}>
          <LoginIllustrationWrapper>
            <Box>
              <img src={LoginIllustrationSrc} alt='login-illustration' />
            </Box>
          </LoginIllustrationWrapper>
        </Grid>
      ) : null}
      <Grid item xs={12} sm={6}>
        <Box
          sx={{
            display: 'flex',
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
            width: '100%'
          }}
        >
          <Box
            sx={{
              width: ['272px', '235px']
            }}
          >
            <form noValidate autoComplete='off' onSubmit={handleSubmit(handleLogin)}>
              <FormControl sx={{ display: 'flex', my: 4 }}>
                <Controller
                  name='email'
                  control={control}
                  render={({ field: { value, onChange, onBlur } }) => (
                    <Input
                      autoFocus
                      label='E-mail'
                      value={value}
                      onBlur={onBlur}
                      onChange={onChange}
                      error={Boolean(errors.email)}
                      placeholder='Digite o seu e-mail'
                    />
                  )}
                />
                {errors?.email && (
                  <FormHelperText sx={{ color: 'error.main' }}>
                    {errors?.email?.message}
                  </FormHelperText>
                )}
              </FormControl>
              <FormControl sx={{ display: 'flex', my: 4 }}>
                <Controller
                  name='password'
                  control={control}
                  rules={{ required: true }}
                  render={({ field: { value, onChange, onBlur } }) => (
                    <Input
                      icon
                      id='password'
                      label='Senha'
                      value={value}
                      onBlur={onBlur}
                      onChange={onChange}
                      error={Boolean(errors.password)}
                      placeholder='Digite sua senha'
                      type={showPassword ? 'text' : 'password'}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            edge='end'
                            onClick={handleClickShowPassword}
                            aria-label='toggle password visibility'
                          >
                            {showPassword ? <EyeOutline /> : <EyeOffOutline fontSize='small' />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  )}
                />
                {errors.password && (
                  <FormHelperText sx={{ color: 'error.main' }}>
                    {errors.password.message}
                  </FormHelperText>
                )}
              </FormControl>
              <Button variant='contained' fullWidth type='submit'>
                Entrar
              </Button>
            </form>
          </Box>
        </Box>
      </Grid>
    </Grid>
  )
}

export default Login
