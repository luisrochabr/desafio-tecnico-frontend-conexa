// ** Hooks
import { useConsultation } from '../hooks/useConsultation'

// ** Third Party Imports
import { Outlet } from 'react-router-dom'

// ** Components Imports
import Footer from './Footer'
import AppBarLayout from './AppBarLayout'
import ModalConsultation from '../views/ModalConsultation'

// ** Styled Imports
import { BlankLayoutWithAppBarWrapper, MainContent } from './styles'

const BlankLayoutWithAppBar = () => {
  const { showModalConsultation } = useConsultation()
  return (
    <BlankLayoutWithAppBarWrapper>
      <AppBarLayout />
      <MainContent>
        <Outlet />
      </MainContent>
      <Footer />
      {showModalConsultation && <ModalConsultation />}
    </BlankLayoutWithAppBarWrapper>
  )
}

export default BlankLayoutWithAppBar
