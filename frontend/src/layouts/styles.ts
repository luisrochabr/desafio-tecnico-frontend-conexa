import { styled } from '@mui/material/styles'

import { Box, BoxProps, Typography, TypographyProps } from '@mui/material'

export const BlankLayoutWithAppBarWrapper = styled(Box)<BoxProps>(({ theme }) => ({
  height: '100vh',
  background: theme.palette.background.default,
  color: theme.palette.text.primary
}))

export const MainContent = styled(Box)<BoxProps>(({ theme }) => ({
  overflowX: 'hidden',
  position: 'relative',
  minHeight: `calc(100vh - ${theme.spacing((theme.mixins.toolbar.minHeight as number) / 4)})`
}))

export const LoginPageTitle = styled(Typography)<TypographyProps>(({ theme }) => ({
  color: theme.palette.primary.main,
  fontWeight: 700,
  fontFamily: 'Montserrat',
  fontSize: '3rem',
  [theme.breakpoints.down('sm')]: {
    fontSize: '2rem'
  }
}))

export const ConsultationPageTitle = styled(Typography)<TypographyProps>(({ theme }) => ({
  color: theme.palette.primary.main,
  fontWeight: 700,
  fontFamily: 'Nunito',
  fontSize: '3rem',
  [theme.breakpoints.down('sm')]: {
    fontSize: '2rem'
  }
}))
