// ** MUI Imports
import {
  AppBar,
  Box,
  Button,
  Stack,
  Theme,
  Toolbar,
  Typography,
  useMediaQuery,
  useTheme
} from '@mui/material'

// ** Hooks Imports
import { useAuth } from '../../hooks/useAuth'

// ** Assets Imports
import Logo from '../../assets/logo-conexa.svg'

const AppBarLayout = () => {
  // ** States
  const { isAuthenticated, user, logout } = useAuth()

  // ** Vars
  const theme = useTheme()
  const hidden = useMediaQuery((theme: Theme) => theme.breakpoints.down('sm'))
  return (
    <AppBar
      elevation={2}
      sx={{
        position: 'sticky',
        background: theme => theme.palette.common.white,
        boxShadow: '4px 4px 12px rgba(0, 0, 0, 0.05)',
        margin: 0,
        padding: 0
      }}
    >
      <Toolbar
        sx={{
          alignContent: 'center',
          justifyContent: isAuthenticated ? ['space-between'] : ['center', 'space-between'],
          padding: theme => `${theme.spacing(0, 2)} !important`
        }}
      >
        <img src={Logo} alt='Logo Conexa' />
        {isAuthenticated && (
          <Stack direction='row' justifyContent='space-between' alignItems='center'>
            <Box sx={{ mr: 1 }}>
              {!hidden && (
                <Typography variant='body1' color={theme.palette.text.primary}>
                  Olá, Dr. {user?.name}
                </Typography>
              )}
            </Box>

            <Button variant='outlined' onClick={logout}>
              Sair
            </Button>
          </Stack>
        )}
      </Toolbar>
    </AppBar>
  )
}

export default AppBarLayout
