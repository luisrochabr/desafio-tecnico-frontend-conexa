// ** Hooks Imports
import { Button } from '@mui/material'
import { useAuth } from '../../hooks/useAuth'
import { useConsultation } from '../../hooks/useConsultation'

// ** Styled Imports
import { Container } from './styles'

const Footer = () => {
  // ** Hooks
  const { isAuthenticated } = useAuth()
  const { setShowModalConsultation } = useConsultation()

  return (
    <>
      {isAuthenticated ? (
        <Container>
          <Button variant='outlined'>Ajuda</Button>
          <Button variant='contained' onClick={() => setShowModalConsultation(true)}>
            Agendar consulta
          </Button>
        </Container>
      ) : null}
    </>
  )
}

export default Footer
