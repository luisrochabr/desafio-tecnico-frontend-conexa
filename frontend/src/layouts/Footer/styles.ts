import { Box, BoxProps } from '@mui/material'
import { styled } from '@mui/material/styles'

export const Container = styled(Box)<BoxProps>(({ theme }) => ({
  display: 'flex',
  justifyContent: 'space-between',
  bottom: 0,
  position: 'sticky',
  background: theme.palette.common.white,
  padding: theme.spacing(4, 2),
  [theme.breakpoints.down('sm')]: {
    borderTop: '2px solid #DAD2D0'
  }
}))
