import React, { lazy } from 'react'

import { Routes, Route } from 'react-router-dom'
import FallbackSpinner from '../components/FallbackSpinner'

const AuthGuard = lazy(async () => import('./AuthGuard'))
const BlankLayoutWithAppBar = lazy(async () => import('../layouts'))
const Login = lazy(async () => import('../pages/login'))
const Home = lazy(async () => import('../pages/home'))

const AppRoutes = () => {
  return (
    <React.Suspense fallback={<FallbackSpinner />}>
      <Routes>
        <Route path='/' element={<BlankLayoutWithAppBar />}>
          <Route path='/' element={<Login />} />
          <Route path='/' element={<AuthGuard />}>
            <Route path='/home' element={<Home />} />
          </Route>
        </Route>
      </Routes>
    </React.Suspense>
  )
}

export default AppRoutes
