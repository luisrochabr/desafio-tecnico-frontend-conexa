export default {
  loginEndpoint: '/login',
  getConsultations: '/consultations?_expand=patient',
  getPatients: '/patients',
  createConsultations: '/consultations',
  storageTokenKeyName: 'accessToken'
}
