export const getErrorMessage = (status: number) => {
  switch (status) {
    case 404:
      return 'E-mail não casdastrado.'
    case 401:
      return 'E-mail ou senha inválido(s).'
    case 403:
      return 'Acesso não autorizado.'
    default:
      return 'Ocorreu um erro, tente novamente mais tarde.'
  }
}
