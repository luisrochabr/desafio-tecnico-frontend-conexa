import { useContext } from 'react'
import { ConsultationContext } from '../contexts/consultationContext'

export const useConsultation = () => useContext(ConsultationContext)
