import { createTheme } from '@mui/material'

export const defaultTheme = createTheme({
  palette: {
    primary: {
      light: '#2E50D4',
      main: '#1C307F'
    },
    background: {
      default: '#FFFFFB'
    },
    text: {
      primary: '#575453',
      secondary: '#999392'
    }
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: ({ ownerState, theme }) => ({
          ...(ownerState.variant === 'contained' &&
            ownerState.color === 'primary' && {
              backgroundColor: theme.palette.primary.light,
              color: theme.palette.common.white,
              textTransform: 'capitalize',
              borderRadius: '8px'
            }),
          ...(ownerState.variant === 'outlined' &&
            ownerState.color === 'primary' && {
              backgroundColor: 'transparent',
              color: theme.palette.primary.light,
              textTransform: 'capitalize',
              borderRadius: '8px',
              border: '2px solid'
            })
        })
      }
    }
  }
})
