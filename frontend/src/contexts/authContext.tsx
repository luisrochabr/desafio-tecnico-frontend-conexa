// ** React Imports
import { createContext, useEffect, useState } from 'react'

// ** ApiClient Import
import { api } from '../services/api'

// ** Common Imports
import config from '../common/constants'
import { getErrorMessage } from '../common/getErrorMessage'

// ** Types
import {
  AuthValuesType,
  LoginDataType,
  ErrCallbackType,
  UserDataType,
  AuthProviderProps
} from '../@types/auth.types'

// ** Third Party Imports
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'

// ** Defaults
const defaultProvider: AuthValuesType = {
  user: null,
  loading: true,
  isAuthenticated: false,
  setIsAuthenticated: () => Boolean,
  setUser: () => null,
  setLoading: () => Boolean,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve()
}

const AuthContext = createContext(defaultProvider)

const handleError = (status: number) => {
  toast.error(getErrorMessage(status))
}

const AuthProvider = ({ children }: AuthProviderProps) => {
  // ** States
  const [user, setUser] = useState<UserDataType | null>(null)
  const [loading, setLoading] = useState<boolean>(true)
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(true)

  // ** Hooks
  const navigate = useNavigate()

  useEffect(() => {
    const data = JSON.parse(window.localStorage.getItem(config.storageTokenKeyName)!)
    if (data?.token) {
      setUser(data)
      setIsAuthenticated(true)
      navigate('/home')
    } else {
      handleLogout()
    }
  }, [])

  const handleLogin = (params: LoginDataType, errorCallback?: ErrCallbackType) => {
    api
      .post(config.loginEndpoint, params)
      .then(async ({ data }) => {
        window.localStorage.setItem(config.storageTokenKeyName, JSON.stringify(data))
        setUser(data)
        setIsAuthenticated(true)
        api.defaults.headers.common['Authorization'] = `Bearer ${data?.token}`
        navigate('/home')
      })
      .catch(err => {
        handleError(err?.response?.status)
        if (errorCallback) errorCallback(err)
      })
  }

  const handleLogout = () => {
    setLoading(false)
    setUser(null)
    setIsAuthenticated(false)
    window.localStorage.clear()
    navigate('/')
  }

  const values = {
    user,
    loading,
    isAuthenticated,
    setUser,
    setLoading,
    setIsAuthenticated,
    login: handleLogin,
    logout: handleLogout
  }

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>
}

export { AuthContext, AuthProvider }
