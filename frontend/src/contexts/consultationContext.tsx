// ** React Imports
import { createContext, useCallback, useState } from 'react'

// ** ApiClient Import
import { api } from '../services/api'

// ** Common Imports
import config from '../common/constants'
import { getErrorMessage } from '../common/getErrorMessage'

// ** Types
import {
  ConsultationValuesType,
  ErrCallbackType,
  ConsultationProviderProps,
  SchedulingDataType,
  PatientDataType,
  CreateSchedulingDataType
} from '../@types/constultation.types'

import { formatDate, formatHour } from '../common/format'

//* * Third Party Imports
import { toast } from 'react-toastify'

// ** Defaults
const defaultProvider: ConsultationValuesType = {
  loading: true,
  schedulingList: [],
  patientList: [],
  showModalConsultation: false,
  newScheduling: null,
  setNewScheduling: () => null,
  setLoading: () => Boolean,
  setShowModalConsultation: () => Boolean,
  handleCreateScheduling: () => Promise.resolve(),
  listAllSchedulings: () => Promise.resolve(),
  listAllPatitients: () => Promise.resolve()
}

const ConsultationContext = createContext(defaultProvider)

const handleError = (status: number) => {
  toast.error(getErrorMessage(status))
}

const ConsultationProvider = ({ children }: ConsultationProviderProps) => {
  // ** States
  const [loading, setLoading] = useState<boolean>(true)
  const [showModalConsultation, setShowModalConsultation] = useState<boolean>(false)
  const [schedulingList, setSchedulingList] = useState<SchedulingDataType[]>([])
  const [patientList, setPatientList] = useState<PatientDataType[]>([])
  const [newScheduling, setNewScheduling] = useState<CreateSchedulingDataType | null>(null)

  const fetchPatientList = useCallback(async () => {
    const { token } = JSON.parse(localStorage.getItem(config.storageTokenKeyName)!)
    api
      .get<PatientDataType[]>(config.getPatients, {
        headers: { Authorization: token }
      })
      .then(({ data }) => setPatientList(data))
      .catch(err => handleError(err?.response?.status))
  }, [])

  const fetchSchedulingList = useCallback(async () => {
    const { token } = JSON.parse(localStorage.getItem(config.storageTokenKeyName)!)
    try {
      const { data } = await api.get<SchedulingDataType[]>(config.getConsultations, {
        headers: { Authorization: token }
      })

      const schedulings = data?.map((scheduling: SchedulingDataType) => {
        const date = formatDate(scheduling.date)
        const hour = formatHour(scheduling.date)
        return {
          ...scheduling,
          date: `${date} às ${hour}`
        }
      })
      setSchedulingList(schedulings)
    } catch (err: any) {
      handleError(err?.response?.status)
    }
  }, [])

  const createScheduling = useCallback(
    async (params: CreateSchedulingDataType, errorCallback?: ErrCallbackType) => {
      const { token } = JSON.parse(localStorage.getItem(config.storageTokenKeyName)!)
      api
        .post(config.createConsultations, params, {
          headers: { Authorization: token }
        })
        .then(res => {
          if (res.status === 201) {
            toast.success('Agendamento criado com sucesso.')
            fetchSchedulingList()
            setShowModalConsultation(false)
          }
        })
        .catch(err => {
          toast.error('Não foi possível criar agendamento, tente novamente')
          if (errorCallback) errorCallback(err)
        })
    },
    [fetchSchedulingList]
  )

  const values = {
    loading,
    schedulingList,
    patientList,
    showModalConsultation,
    newScheduling,
    setNewScheduling,
    setLoading,
    setShowModalConsultation,
    handleCreateScheduling: createScheduling,
    listAllSchedulings: fetchSchedulingList,
    listAllPatitients: fetchPatientList
  }

  return <ConsultationContext.Provider value={values}>{children}</ConsultationContext.Provider>
}

export { ConsultationContext, ConsultationProvider }
