import Modal from '../../components/Modal'
import { useConsultation } from '../../hooks/useConsultation'
import Input from '../../components/Input'
import {
  Autocomplete,
  createFilterOptions,
  Grid,
  TextField,
  Typography,
  Button
} from '@mui/material'
import { PatientDataType } from '../../@types/constultation.types'

const filterOptions = createFilterOptions({
  matchFrom: 'start',
  stringify: (option: PatientDataType) => option.first_name
})

const ModalConsultation = () => {
  // ** States
  const {
    showModalConsultation,
    setShowModalConsultation,
    patientList,
    handleCreateScheduling,
    newScheduling,
    setNewScheduling
  } = useConsultation()

  const handleNewScheduling = () => {
    if (newScheduling) {
      handleCreateScheduling(newScheduling)
    }
  }
  return (
    <Modal isOpen={showModalConsultation} setIsOpen={setShowModalConsultation}>
      <Grid container spacing={2}>
        <Grid item xs={12} alignContent='center'>
          <Typography variant='h6'>Novo Agendamento</Typography>
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            id='scheduling-patient'
            options={patientList}
            filterOptions={filterOptions}
            getOptionLabel={option => option.first_name}
            renderInput={params => <TextField {...params} label='Pacientes' variant='standard' />}
            noOptionsText='Nenhum paciente encontrado.'
            onChange={(event, value) =>
              setNewScheduling({ ...newScheduling, patientId: value?.id })
            }
          />
        </Grid>
        <Grid item xs={6}>
          <Input
            id='scheduling-date-time'
            label='Data do Atendimento'
            type='datetime-local'
            onChange={e => setNewScheduling({ ...newScheduling, date: new Date(e.target.value) })}
          />
        </Grid>
        <Grid item xs={12}>
          <Button variant='contained' fullWidth onClick={() => handleNewScheduling()}>
            Agendar
          </Button>
        </Grid>
      </Grid>
    </Modal>
  )
}

export default ModalConsultation
