import { Box, Typography } from '@mui/material'
import Plant from '../../assets/illustration-plant.svg'
import Certs from '../../assets/illustration-certificates.svg'

const NoSchedulings = () => {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-end'
        }}
      >
        <img src={Certs} />
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          flexWrap: 'wrap'
        }}
      >
        <Typography variant='body1' textAlign='center'>
          Não há nenhuma <br /> consulta agendada
        </Typography>
      </Box>
      <Box>
        <img src={Plant} />
      </Box>
    </>
  )
}

export default NoSchedulings
