import { ReactNode } from 'react'
import { InputProps as MuiInputProps } from '@mui/material'

export interface InputProps extends MuiInputProps {
  icon?: boolean
  label: string
}

export interface IModalProps {
  children: ReactNode
  isOpen: boolean
  setIsOpen: (value: boolean) => void
}
