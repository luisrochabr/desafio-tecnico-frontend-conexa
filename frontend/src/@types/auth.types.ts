import { ReactNode } from 'react'
import * as yup from 'yup'

export const loginSchema = yup.object({
  email: yup.string().email('Forneça um e-mail válido').required('Forneça o e-mail cadastrado'),
  password: yup
    .string()
    .min(6, 'Ops...não esqueça da senha.')
    .required('Não esqueça de digitar sua senha :)')
})

export type LoginDataType = yup.InferType<typeof loginSchema>

export type AuthProviderProps = {
  children: ReactNode
}

export type ErrCallbackType = (err: { [key: string]: string }) => void

export type UserDataType = {
  name: string
}

export type AuthValuesType = {
  loading: boolean
  user: UserDataType | null
  isAuthenticated: boolean
  setLoading: (value: boolean) => void
  setIsAuthenticated: (value: boolean) => void
  logout: () => void
  setUser: (value: UserDataType | null) => void
  login: (params: LoginDataType, errorCallback?: ErrCallbackType) => void
}
