import { ReactNode } from 'react'

export type ErrCallbackType = (err: { [key: string]: string }) => void

export type ConsultationProviderProps = {
  children: ReactNode
}

export type PatientDataType = {
  id: number
  first_name: string
  last_name: string
  email: string
}

export type SchedulingDataType = {
  id?: number
  patientId: number
  date: string
  patient?: PatientDataType
}

export type CreateSchedulingDataType = {
  patientId?: number | null
  date?: Date | string | null
}

export type ConsultationValuesType = {
  loading: boolean
  schedulingList: SchedulingDataType[]
  patientList: PatientDataType[]
  showModalConsultation: boolean
  newScheduling: CreateSchedulingDataType | null
  setNewScheduling: (value: CreateSchedulingDataType | null) => void
  setLoading: (value: boolean) => void
  setShowModalConsultation: (value: boolean) => void
  handleCreateScheduling: (params: CreateSchedulingDataType) => Promise<void>
  listAllSchedulings: () => Promise<void>
  listAllPatitients: () => Promise<void>
}
