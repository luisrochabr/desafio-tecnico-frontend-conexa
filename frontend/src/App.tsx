// ** MUI Imports
import { StyledEngineProvider, ThemeProvider } from '@mui/material'

// ** Third Party Imports
import { BrowserRouter as Router } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import 'react-virtualized/styles.css'

// ** Providers Imports
import { AuthProvider } from './contexts/authContext'
import { ConsultationProvider } from './contexts/consultationContext'

// ** Custom Components Imports
import { defaultTheme } from './styles/theme/default'
import AppRoutes from './routes'

function App() {
  return (
    <Router>
      <AuthProvider>
        <ConsultationProvider>
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={defaultTheme}>
              <AppRoutes />
              <ToastContainer />
            </ThemeProvider>
          </StyledEngineProvider>
        </ConsultationProvider>
      </AuthProvider>
    </Router>
  )
}

export default App
