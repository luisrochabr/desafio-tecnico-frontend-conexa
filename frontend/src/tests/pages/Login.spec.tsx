import React from 'react'
import { render, screen } from '@testing-library/react'
import Login from '../../pages/login'

// not testing for mobile as default
jest.mock('@mui/material', () => ({
  ...jest.requireActual('@mui/material'),
  useMediaQuery: jest.fn().mockReturnValue(false)
}))

describe('Login page', () => {
  it('renders correctly', () => {
    render(<Login />)

    expect(screen.getByText('Faça Login')).toBeInTheDocument()
  })
})
