import React from 'react'
import { render, screen } from '@testing-library/react'
import Home from '../../pages/home'

describe('Home page', () => {
  it('renders correctly', () => {
    render(<Home />)

    expect(screen.getByText('Consultas')).toBeInTheDocument()
  })
})
